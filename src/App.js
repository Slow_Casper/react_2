import { Component } from "react";
import './index.css'
import List from "./Components/List";
import Header from "./Components/Header";


class App extends Component {

  state = {
    wishList: localStorage.wishList ? JSON.parse(localStorage.wishList) : [],
    cartList: localStorage.cartList ? JSON.parse(localStorage.cartList) : [],
    store: [],
  }

  componentDidMount() {
    fetch("./store/store.json")
      .then((res) => res.json())
      .then((data) => {
        this.setState((prev) => ({...prev, store: [...data]}))
      })
    
    if(!localStorage.cartList && !localStorage.wishList) {
      localStorage.cartList = JSON.stringify([])
      localStorage.wishList = JSON.stringify([])
    } 
  }

  cartAndWish() {
    const cartList = localStorage.cartList ? JSON.parse(localStorage.cartList) : []
    const wishList = localStorage.wishList ? JSON.parse(localStorage.wishList) : []
    this.setState((prev) => ({...prev, cartList: cartList, wishList: wishList}))
  }

  render() {
    return (
      <div className="app">
        <Header wishList={this.state.wishList} cartList={this.state.cartList}/>
        <List store={this.state.store} cartAndWish={() => this.cartAndWish()} />
      </div>
    );
  }
}

export default App;
